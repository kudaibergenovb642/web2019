var triangleArea = function() {
    var base = parseFloat (document.getElementById('base').value);
    var height = parseFloat (document.getElementById('height').value);
    var output = document.getElementById('output');
    if (isNaN(base)  ||isNaN(height)) {
        output.textContent = "Write data triangle.";
    } else {
        var area = 1/2*base*height;
        output.textContent = "S =" + area;
    }
};